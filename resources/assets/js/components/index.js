import Vue from 'vue'
import VueFeather from 'vue-feather';

import Card from './Card'
import Child from './Child'
import Button from './Button'
import Checkbox from './Checkbox'
import { HasError, AlertError, AlertSuccess } from 'vform'

// Components that are registered globaly.
[
  Card,
  Child,
  Button,
  Checkbox,
  HasError,
  AlertError,
  VueFeather,
  AlertSuccess,
].forEach(Component => {
  Vue.component(Component.name, Component)
})
