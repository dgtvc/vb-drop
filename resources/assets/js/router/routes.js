//import Welcome from '~/pages/welcome';
// const Welcome = () => import('~/pages/welcome').then(m => m.default || m)
import Login from'~/pages/auth/login'
import Register from'~/pages/auth/register'
import PasswordEmail from'~/pages/auth/password/email'
import PasswordReset from'~/pages/auth/password/reset'
import NotFound from'~/pages/errors/404'

import Home from'~/pages/home'
import Case from'~/pages/case'
import Settings from'~/pages/settings/index'
import SettingsProfile from'~/pages/settings/profile'
import SettingsPassword from'~/pages/settings/password'
import FAQ from'~/pages/faq'
import Support from'~/pages/support/support'
import SupportOpen from'~/pages/support/supportopen'
import Account from'~/pages/account/account'
import Payments from'~/pages/payments/'
import Free from'~/pages/free/'
// const Login = () => import('~/pages/auth/login').then(m => m.default || m)
// const Register = () => import('~/pages/auth/register').then(m => m.default || m)
// const PasswordEmail = () => import('~/pages/auth/password/email').then(m => m.default || m)
// const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
// const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)
//
// const Home = () => import('~/pages/home').then(m => m.default || m)
// const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
// const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
// const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m)

export default [
  { path: '/', name: 'welcome', component: Home },
  { path: '/case/:id', name: 'case', component: Case, props: true },
  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/faq', name: 'faq', component: FAQ },
  { path: '/support', name: 'support', component: Support },
  { path: '/supportopen', name: 'supportopen', component: SupportOpen },
  { path: '/account', name: 'account', component: Account },
  { path: '/payments', name: 'payments', component: Payments },
  { path: '/free', name: 'free', component: Free },
  { path: '/password/reset', name: 'password.request', component: PasswordEmail },
  { path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },
  { path: '/settings',
    component: Settings,
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: SettingsProfile },
      { path: 'password', name: 'settings.password', component: SettingsPassword }
    ] },

  { path: '*', component: NotFound }
]
