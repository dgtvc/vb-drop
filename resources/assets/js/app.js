import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'

import '~/plugins'
import '~/components'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    sockets: {
        connect: function () {
            console.log('socket connected')
            store.dispatch('socket/connect')
        },
        disconnect: function() {
            console.log('socket connected')
            store.dispatch('socket/disconnect')
        },
        customEmit: function (val) {
            console.log('this method was fired by the socket server. eg: io.emit("customEmit", data)')
        }
    },
    i18n,
    store,
    router,
    ...App
})
