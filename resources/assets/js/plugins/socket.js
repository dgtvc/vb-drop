import io from 'socket.io-client'

const options = {
    broadcaster: 'socket.io',
    host: `https://vb.dgtvc.pl:2083`,
    namespace: null,
    key: null,
};

export default io(options.host, options);