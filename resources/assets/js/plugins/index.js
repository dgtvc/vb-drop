import './axios'
import './fontawesome'
import socket from './socket'
import 'bootstrap'

import Vue from "vue";
import VueSocketio from "vue-socket.io-extended";
import Store from '~/store';

Vue.use(VueSocketio, socket, { store: Store });

