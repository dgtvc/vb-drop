// auth.js
export const LOGOUT = 'LOGOUT'
export const SAVE_TOKEN = 'SAVE_TOKEN'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'
export const UPDATE_USER = 'UPDATE_USER'

// lang.js
export const SET_LOCALE = 'SET_LOCALE'

// drops.js
export const ADD_DROP = 'ADD_DROP'
export const REDUCE_DROP = 'REDUCE_DROP'
export const DROP_TOGGLE = 'DROP_TOGGLE'

// case.js
export const FETCH_CASES = 'FETCH_CASES'
export const FETCH_CASES_FAILURE = 'FETCH_CASES_FAILURE'
