import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  list: [],
}

// getters
export const getters = {
  list: state => state.list,
}

// mutations
export const mutations = {
  [types.FETCH_CASES] (state, { data }) {
    state.list = data.list;
  },
  [types.FETCH_CASES_FAILURE] (state) {
    state.list = [];
  },
}

// actions
export const actions = {
  async fetchCases ({ commit }) {
    try {
      const { data } = await axios.get('/api/cases')

      commit(types.FETCH_CASES, { data })
    } catch (e) {
      commit(types.FETCH_CASES_FAILURE)
    }
  },
}
