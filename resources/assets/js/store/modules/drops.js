import * as types from '../mutation-types'

// state
export const state = {
     drops: [

         {
             user: {
                 id: 123,
                 name: 'test',
                 avatar: 'https://vbcase.com/uploads/profile/116789537025745564670.jpg'
             },
             drop: {
                 id: 123,
                 value: 123,
                 chance: 1.23,
                 image: '/images/vb-icon.png'
             }
         },
         {
             user: {
                 id: 123,
                 name: 'test',
                 avatar: 'https://vbcase.com/uploads/profile/116789537025745564670.jpg'
             },
             drop: {
                 id: 123,
                 value: 123,
                 chance: 1.23,
                 image: '/images/vb-icon.png'
             }
         },

         {
             user: {
                 id: 123,
                 name: 'test',
                 avatar: 'https://vbcase.com/uploads/profile/116789537025745564670.jpg'
             },
             drop: {
                 id: 123,
                 value: 123,
                 chance: 1.23,
                 image: '/images/vb-icon.png'
             }
         }

         /*
         {
            user: {
                id: 123,
                name: 'test',
                avatar: '232'
            },
            drop: {
                id: 123,
                value: 123,
                chance: 1.23,
                image: 'dsads'
            }
         }
          */

     ],
    count: 30,
    active: (localStorage.getItem('drop') ? false: true)
}

// getters
export const getters = {
    drops: state => state.drops,
    active: state => state.active,
}

// mutations
export const mutations = {
    [types.ADD_DROP] (state, { drop }) {
        state.drops.push(drop);
    },

    [types.REDUCE_DROP] (state) {

        let drops_length = state.drops.length;

        let to_cut = drops_length - state.count;

        state.drops = state.drops.filter((el, index) => index < to_cut);
    },
    [types.DROP_TOGGLE] (state) {
        if(state.active)
            localStorage.removeItem('drop');
        else
            localStorage.setItem('drop', true);

        state.active = !state.active

    }
}

// actions
export const actions = {
    addDrop({ commit }, payload) {
        commit(types.ADD_DROP, payload)
    },

    toggle({ commit }) {
        commit(types.DROP_TOGGLE);
    },

    reduce({ commit }) {
        commit(types.REDUCE_DROP);
    }
}
