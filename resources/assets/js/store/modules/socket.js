import * as types from '../mutation-types'
import socket from '~/plugins/socket';

export const state = {
    connect: false,
    messages: []
}

// getters
export const getters = {
    connect: state => state.connect
}


// mutations
export const mutations = {
    SOCKET_CONNECT: (state, status) => {
        state.connect = true;
    },
    SOCKET_DISCONNECT: (state, status) => {
        state.connect = false;
    },
    SOCKET_USER_MESSAGE: (state, message) => {
        state.message = message;
    },
    SOCKET_CHAT_MESSAGE(state, message) {
        state.messages.push(message);
    }
}

// actions
export const actions = {
    connect: ({commit}) => {
        commit('SOCKET_CONNECT');
    },
    disconnect: ({commit}) => {
        commit('SOCKET_DISCONNECT');
    },

    logout: ({commit}) => {

        socket.emit('logout');

    },

    socket_userMessage: ({commit, dispatch}, message) => {
        context.dispatch('newMessage', message);
        context.commit('NEW_MESSAGE_RECEIVED', message);
        if (message.is_important) {
            context.dispatch('alertImportantMessage', message);
        }
    },

    socket_checkAuth: ({commit, dispatch, rootState}) => {

        let user = rootState.auth.user

        if(user) {

            let token = rootState.auth.token

            socket.emit('authenticate',{
                token
            });

        }

    },

    socket_chatMessage() {
        console.log('this action will be called');
    }

}
