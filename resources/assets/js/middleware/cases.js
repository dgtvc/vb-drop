import store from '~/store'

export default async (to, from, next) => {

    if(!store.getters['case/list'].length) {
        try {
            await store.dispatch('case/fetchCases')
        } catch (e) {
        }
    }

    next()
}
