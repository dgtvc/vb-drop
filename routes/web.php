<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['rank:6'], 'prefix' => 'admin', 'name' => 'admin.'], function() {
    Route::get('/', 'AdminController@dashboard')->name('dashboard');
    Route::resource('roles','Admin\RoleController');
    Route::resource('users','Admin\UserController');
    Route::resource('psc','Admin\PSCController');
    Route::resource('pscgroups','Admin\PSCGroupController');
    // Route::resource('tickets','Admin\TicketController');
    Route::resource('case','Admin\CaseController');
    Route::get('tickets', 'Admin\TicketsController@index')->name('tickets.admin');
    Route::post('close_ticket/{ticket_id}', 'Admin\TicketsController@close');
    Route::get('tickets/{ticket_id}', 'Admin\TicketsController@ashow');
    Route::post('comment', 'Admin\CommentsController@apostComment');
});

Route::get('/', function () {
    return view('index');
})->name('home');

Route::get('{path}', function () {
    return view('index');
})->where('path', '(.*)');
