<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cases extends Model
{
    use SoftDeletes;

    protected $fillable = ['case_name' , 'case_price', 'case_priority' , 'case_image' , 'case_active'];

    public function items() {
        return $this->hasMany(CaseItem::class, 'case_id', 'id');
    }
}
