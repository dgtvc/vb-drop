<?php
/**
 * Created by PhpStorm.
 * User: dgtcx
 * Date: 07.10.2018
 * Time: 23:04
 */

namespace App\Http\Middleware;

use Closure;

class RankMiddleware
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        dd($request->user());

        if(! $request->user())
            return redirect()->route('home');

        if ($request->user()->rank < $role)
            return redirect()->route('home');

        return $next($request);
    }

}