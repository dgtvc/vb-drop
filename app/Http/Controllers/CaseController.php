<?php

namespace App\Http\Controllers;

use App\Models\Cases;
use Illuminate\Http\Request;

class CaseController extends Controller
{

    public function index() {

        $model = Cases::query();

        $model->orderBy('case_priority');

        $items = $model->get();

        return response()->json([
            'success' => true,
            'list' => $items
        ]);
    }

    public function show(Request $request, $id) {

        $model = Cases::find($id);

        $model->items = $model->items()->get();

        return response()->json([
            'success' => true,
            'item' => $model
        ]);


    }

}
